
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Mapping.Orders
{
    public partial class OrderItemMap : UrsEntityTypeConfiguration<OrderItem>
    {
        public override void Configure(EntityTypeBuilder<OrderItem> builder)

        {
            builder.ToTable(nameof(OrderItem));
            builder.HasKey(opv => opv.Id);
            builder.Property(opv=>opv.GoodsName).IsRequired().HasMaxLength(400);
            builder.Property(opv => opv.AttributeDescription);
            builder.Property(opv => opv.AttributesXml);
            builder.Property(opv => opv.UnitPrice).HasColumnType("decimal(18, 4)");
            builder.Property(opv => opv.Price).HasColumnType("decimal(18, 4)");
            builder.Property(opv => opv.ItemWeight).HasColumnType("decimal(18, 4)");

            builder.HasOne(opv => opv.Order)
                .WithMany(o => o.orderItems)
                .HasForeignKey(opv => opv.OrderId);

            builder.HasOne(opv => opv.Goods)
                .WithMany()
                .HasForeignKey(opv => opv.GoodsId);
            base.Configure(builder);
        }
    }
}