
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsParameterOptionMap : UrsEntityTypeConfiguration<GoodsParameterOption>
    {
        public override void Configure(EntityTypeBuilder<GoodsParameterOption> builder)
        {
            builder.ToTable(nameof(GoodsParameterOption));
            builder.HasKey(sao => sao.Id);
            builder.Property(sao => sao.Name).IsRequired();
            
            builder.HasOne(sao => sao.Mapping)
                .WithMany(sa => sa.GoodsParameterOptions)
                .HasForeignKey(sao => sao.GoodsParameterId);
            base.Configure(builder);
        }
    }
}