﻿using Urs.Core;
using Urs.Core.Data;

namespace Urs.Data
{
    /// <summary>
    /// Represents the Entity Framework data provider manager
    /// </summary>
    public partial class EfDataProviderManager : IDataProviderManager
    {
        #region Properties

        /// <summary>
        /// Gets data provider
        /// </summary>
        public IDataProvider DataProvider
        {
            get
            {
                return new MySqlDataProvider();
            }
        }

        #endregion
    }
}