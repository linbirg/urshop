﻿
using System.Collections.Generic;
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class CommonSettings : ISettings
    {
        public bool UseSystemEmailForContactUsForm { get; set; }

        public bool UseStoredProceduresIfSupported { get; set; }

        public bool HideAdvertisementsOnAdminArea { get; set; }

        public bool SitemapEnabled { get; set; }
        public bool SitemapIncludeCategories { get; set; }
        public bool SitemapIncludeBrands { get; set; }
        public bool SitemapIncludeGoodss { get; set; }
        public bool SitemapIncludeTopics { get; set; }
        /// <summary>
        /// Gets a sets a value indicating whether to display a warning if java-script is disabled
        /// </summary>
        public bool DisplayJavaScriptDisabledWarning { get; set; }
        public bool ErrorFeedbackEnabled { get; set; }
        /// <summary>
        /// Gets a sets a value indicating whether full-text search is supported
        /// </summary>
        public bool UseFullTextSearch { get; set; }

        public string HotSearchPhrases { get; set; }
        public string StaticFilesCacheControl { get; set; }

        public Dictionary<string, string> CustomAttributeArray { get; set; }
        /// <summary>
        /// Gets a sets a value indicating whether 404 errors (page or file not found) should be logged
        /// </summary>
        public bool Log404Errors { get; set; }
    }
}