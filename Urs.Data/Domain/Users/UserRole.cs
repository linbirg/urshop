using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Security;

namespace Urs.Data.Domain.Users
{
    public partial class UserRole : BaseEntity
    {
        /// <summary>
        /// Gets or sets the user role name
        /// </summary>
        public virtual string Name { get; set; }
       
        /// <summary>
        /// Gets or sets a value indicating whether the user role is active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user role is system
        /// </summary>
        public virtual bool IsSystemRole { get; set; }

        /// <summary>
        /// Gets or sets the user role system name
        /// </summary>
        public virtual string SystemName { get; set; }
    }

}