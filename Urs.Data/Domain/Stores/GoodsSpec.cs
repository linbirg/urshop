using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 商品规格
    /// </summary>
    public partial class GoodsSpec : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        public virtual bool Deleted { get; set; }
    }
}
