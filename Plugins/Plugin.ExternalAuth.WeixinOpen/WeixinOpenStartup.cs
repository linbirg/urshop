﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Urs.Core.Infrastructure;
using Urs.Framework.Infrastructure;
using Urs.Plugin.ExternalAuth.SwaggerGen;

namespace ExternalAuth.WeixinOpen
{
    public class WeixinOpenStartup : IUrsStartup
    {
        /// <summary>
        /// Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                options.IncludeXmlComments(Path.Combine(Directory.GetCurrentDirectory(), @"Plugins\ExternalAuth.WeixinOpen\Plugin.WeixinLogin.xml"));
                //添加对控制器的标签(描述)
                options.DocumentFilter<SwaggerDocTag>();
                options.CustomSchemaIds(x => x.FullName);
            });
            services.AddMvcCore().AddApiExplorer();
        }

        /// <summary>
        /// Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseSwagger();
            application.Use(async (context, next) =>
            {
                context.Request.EnableBuffering();
                await next();
            });
        }

        public int Order => 1;
    }
}
