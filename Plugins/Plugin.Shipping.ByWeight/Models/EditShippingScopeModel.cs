﻿using System.Collections.Generic;
using Urs.Framework.Models;

namespace Urs.Plugin.Shipping.ByWeight.Models
{
    public class EditShippingScopeModel : BaseEntityModel
    {
        public EditShippingScopeModel()
        {
            Cities = new List<ShippingCityModel>();
        }
        public string Code { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public bool Selected { get; set; }

        public bool Disabled { get; set; }

        public IList<ShippingCityModel> Cities { get; set; }

        public partial class ShippingCityModel : BaseEntityModel
        {
            public string Code { get; set; }
            public string Name { get; set; }

            public bool Selected { get; set; }
        }
    }
}
