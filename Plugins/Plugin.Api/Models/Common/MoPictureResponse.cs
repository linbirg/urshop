﻿namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 图片信息
    /// </summary>
    public partial class MoPictureResponse
    {
        /// <summary>
        /// 图片Id
        /// </summary>
        public int PictureId { get; set; }
        public string NormalUrl { get; set; }
        public string BigUrl { get; set; }
    }
}