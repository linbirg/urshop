﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 阶梯价
    /// </summary>
    public class MoTierPriceArray
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public MoTierPriceArray()
        {
            Prices = new List<MoTierPrice>();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsId { get; set; }
        /// <summary>
        /// 多层价格
        /// </summary>
        public IList<MoTierPrice> Prices { get; set; }
    }

}