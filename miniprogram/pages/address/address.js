import api from '../../api/api'
import {
  addressList
} from '../../api/conf'
import {
  addressdefault
} from '../../api/conf'

Page({
  data: {
    addressList: []
  },
  onLoad: function(options) {
    
  },
  onReady: function() {
    
  },
  onShow: function() {
    this.getaddress()
  },
  getaddress: function() {
    wx.showNavigationBarLoading()
    var that = this
    api.get(addressList).then((res) => {
      that.setData({
        addressList: res.Data
      })
      wx.hideNavigationBarLoading()
    })
  },
  qiehuan: function(e) {
    var that = this
    api.post(addressdefault + '?addressId=' + e.currentTarget.dataset.id, {}).then((res) => {
      if (res.Code == 200) {
        that.getaddress()
      }
    })
  },
  goEdit: function(e) {
    let obj = JSON.stringify(e.currentTarget.dataset.item)
    wx.navigateTo({
      url: '/pages/address-edit/address-edit?item=' + obj
    });
  },
  deletAdress: function(e) {
    var that = this
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function(sm) {
        if (sm.confirm) {
          api.post('/v1/address/delete?addressId=' + e.currentTarget.dataset.id, {
          }).then(res => {
            if(res.Code == 200){
              that.getaddress()
            }
          })
        }
      }
    })
  }
})