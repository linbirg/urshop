﻿

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Urs.Framework.Localization;
using Urs.Framework.Mvc.Routes;

namespace Urs.Web.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routes)
        {
            //areas
            routes.MapRoute(name: "areaRoute", template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

            //home page
            routes.MapLocalizedRoute("HomePage", "",
                            new { controller = "Home", action = "Index" });

            //home page
            routes.MapLocalizedRoute("UEditor", "UEditor/Do",
                            new { controller = "UEditor", action = "Do" });
            //home page
            routes.MapLocalizedRoute("ScheduleTask", "/ScheduleTask/RunTask",
                            new { controller = "ScheduleTask", action = "RunTask" });
        }

        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
