﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Urs.Admin.Models.Common;
using Urs.Admin.Models.Stores;
using Urs.Core;
using Urs.Data.Domain.Configuration;
using Urs.Data.Domain.Stores;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;
using Urs.Services.Localization;
using Urs.Services.Security;
using Urs.Services.Stores;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class ShopController : BaseAdminController
    {
        #region Fields

        private readonly IShopService _shopService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;

        #endregion

        #region Constructors

        public ShopController(IShopService shopService,
            ILocalizationService localizationService, IPermissionService permissionService,
            AdminAreaSettings adminAreaSettings)
        {
            this._shopService = shopService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._adminAreaSettings = adminAreaSettings;
        }

        #endregion


        #region  shop

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            return View();
        }

        [HttpPost]
        public IActionResult List(PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            var gridModel = new ResponseResult();

            var news = _shopService.GetAllShop(command.Page - 1, command.Limit, published: null);
            gridModel.data = news.Select(x => x.ToModel<ShopModel>());
            gridModel.count = news.TotalCount;

            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult Published(CheckboxModel checkbox)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            var shop = _shopService.GetShopById(checkbox.Id);
            if (shop == null)
                return Json(new { error = 1 });

            if (checkbox.Checked)
                shop.Published = true;
            else
                shop.Published = false;
            _shopService.UpdateShop(shop);

            return Json(new { success = 1 });
        }

        public IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            var shop = _shopService.GetShopById(id);
            if (shop == null)
            {
                var model = new ShopModel();
                return View(model);
            }
            else
            {
                var model = shop.ToModel<ShopModel>();
                return View(model);
            }
        }

        [HttpPost]
        public IActionResult Edit(ShopModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            var shop = _shopService.GetShopById(model.Id);
            if (shop == null)
            {
                shop = model.ToEntity<Shop>();
                if (string.IsNullOrEmpty(shop.Code))
                    shop.Code = CommonHelper.RandomCode(num: 5);
                _shopService.InsertShop(shop);
            }
            else
            {
                shop = model.ToEntity(shop);
                _shopService.UpdateShop(shop);
            }
            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageGoods))
                return HttpUnauthorized();

            var shop = _shopService.GetShopById(id);
            if (shop == null)
                return Json(new { error = 1 });

            _shopService.DeleteShop(shop);

            return Json(new { success = 1 });
        }

        #endregion

    }
}
