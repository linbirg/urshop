﻿using System.ComponentModel.DataAnnotations;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class AdminLoginModel : BaseModel
    {
        public bool CheckoutAsGuest { get; set; }

        
        public string AccountName { get; set; }

        [DataType(DataType.Password)]
        
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}