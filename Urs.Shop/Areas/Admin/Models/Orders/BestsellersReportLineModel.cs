﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class BestsellersReportLineModel : BaseModel
    {
        public int GoodsId { get; set; }
        [UrsDisplayName("Admin.SalesReport.Bestsellers.Fields.Name")]
        public string GoodsName { get; set; }

        [UrsDisplayName("Admin.SalesReport.Bestsellers.Fields.TotalAmount")]
        public string TotalAmount { get; set; }

        [UrsDisplayName("Admin.SalesReport.Bestsellers.Fields.TotalQuantity")]
        public decimal TotalQuantity { get; set; }
    }
}