﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class OrderListModel : BaseModel
    {
        public OrderListModel()
        {
            AvailableOrderStatuses = new List<SelectListItem>();
            AvailablePaymentStatuses = new List<SelectListItem>();
            AvailableShippingStatuses = new List<SelectListItem>();
            AvailablePaymentMethods = new List<SelectListItem>();
        }

        [UrsDisplayName("Admin.Orders.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [UrsDisplayName("Admin.Orders.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [UrsDisplayName("Admin.Orders.List.OrderStatus")]
        public int OrderStatusId { get; set; }
        [UrsDisplayName("Admin.Orders.List.PaymentStatus")]
        public int PaymentStatusId { get; set; }
        [UrsDisplayName("Admin.Orders.List.ShippingStatus")]
        public int ShippingStatusId { get; set; }
        [UrsDisplayName("Admin.Orders.List.ShippingEnabled")]
        public bool ShippingEnabled { get; set; }

        public IList<SelectListItem> AvailableOrderStatuses { get; set; }
        public IList<SelectListItem> AvailablePaymentStatuses { get; set; }
        public IList<SelectListItem> AvailableShippingStatuses { get; set; }
        public IList<SelectListItem> AvailablePaymentMethods { get; set; }
    }
}