﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Localization;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsTagValidator))]
    public partial class GoodsTagModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.GoodsTags.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.GoodsTags.Fields.GoodsCount")]
        public int GoodsCount { get; set; }
    }
}