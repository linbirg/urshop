﻿using System.ComponentModel;
using Urs.Admin.Models.Common;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class ShippingSettingsModel : BaseModel, ISettingsModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.Shipping.Enabled")]
        public bool Enabled { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Shipping.FreeShippingOverXEnabled")]
        public bool FreeShippingOverXEnabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Shipping.FreeShippingOverXValue")]
        public decimal FreeShippingOverXValue { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Shipping.DisplayShipmentEventsToUsers")]
        public bool DisplayShipmentEventsToUsers { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Shipping.ShippingOriginAddress")]
        public AddressModel ShippingOriginAddress { get; set; }
        [DisplayName("物流公司")]
        public string ShippingExpressNames { get; set; }
    }
}