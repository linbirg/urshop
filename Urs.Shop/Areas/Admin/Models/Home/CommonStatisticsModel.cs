﻿using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Home
{
    public partial class CommonStatisticsModel : BaseModel
    {
        public int NumberOfOrders { get; set; }

        public int NumberOfUsers { get; set; }

        public int NumberOfPendingAfterSales { get; set; }

        public int NumberOfLowStockGoodss { get; set; }
    }
}