using System;
using System.Linq;
using Urs.Core;
using Urs.Core.Data;
using Urs.Data.Domain.Common;

namespace Urs.Services.Common
{
    public partial class FavoritesService : IFavoritesService
    {
        #region Fields

        private readonly IRepository<Favorites> _favoritesRepository;
        #endregion

        #region Ctor

        public FavoritesService(IRepository<Favorites> favoritesRepository)
        {
            this._favoritesRepository = favoritesRepository;
        }

        #endregion

        #region Favorites

        public virtual void DeleteFavorites(Favorites favorites)
        {
            if (favorites == null)
                throw new ArgumentNullException("favorites");

            _favoritesRepository.Delete(favorites);

        }

        public virtual Favorites GetById(int favoritesId)
        {
            if (favoritesId == 0)
                return null;

            var favorites = _favoritesRepository.GetById(favoritesId);
            return favorites;
        }

        public virtual IPagedList<Favorites> GetFavorites(int userId, int pageIndex, int pageSize)
        {
            var query = _favoritesRepository.Table;
            if (userId > 0)
                query = query.Where(q => q.UserId == userId);

            query = query.OrderByDescending(b => b.CreateTime);

            var blogPosts = new PagedList<Favorites>(query, pageIndex, pageSize);
            return blogPosts;
        }

        public virtual void InsertFavorites(Favorites favorites)
        {
            if (favorites == null)
                throw new ArgumentNullException("favorites");

            favorites.CreateTime = DateTime.Now;
            _favoritesRepository.Insert(favorites);

        }

        public virtual void UpdateFavorites(Favorites favorites)
        {
            if (favorites == null)
                throw new ArgumentNullException("favorites");

            _favoritesRepository.Update(favorites);

        }
        #endregion

    }
}