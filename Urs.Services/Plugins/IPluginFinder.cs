﻿using System.Collections.Generic;
using Urs.Data.Domain.Users;
using Urs.Core.Plugins;

namespace Urs.Services.Plugins
{
    public interface IPluginFinder
    {
        IEnumerable<string> GetPluginGroups();

        IEnumerable<T> GetPlugins<T>(LoadPluginsMode loadMode = LoadPluginsMode.InstalledOnly,
            User user = null, int storeId = 0, string group = null) where T : class, IPlugin;

        IEnumerable<PluginDescriptor> GetPluginDescriptors(LoadPluginsMode loadMode = LoadPluginsMode.InstalledOnly,
            User user = null, int storeId = 0, string group = null);

        IEnumerable<PluginDescriptor> GetPluginDescriptors<T>(LoadPluginsMode loadMode = LoadPluginsMode.InstalledOnly,
            User user = null, int storeId = 0, string group = null) where T : class, IPlugin;

        PluginDescriptor GetPluginDescriptorBySystemName(string systemName, LoadPluginsMode loadMode = LoadPluginsMode.InstalledOnly);

        PluginDescriptor GetPluginDescriptorBySystemName<T>(string systemName, LoadPluginsMode loadMode = LoadPluginsMode.InstalledOnly)
            where T : class, IPlugin;

        void ReloadPlugins(PluginDescriptor pluginDescriptor);
    }
}
